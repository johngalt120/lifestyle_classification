import os
import torchvision
import torch
import shutil
import numpy as np

from PIL import Image
from datetime import datetime
from torchvision import transforms, models
from tqdm import tqdm


def train_model(model, loss, optimizer, scheduler, train_dataloader, val_dataloader, device, num_epochs):
    best_summary_acc = 0
    save_path = os.path.join('/volume/', str(datetime.now()).replace(' ', '-'))
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    for epoch in range(num_epochs):
        print('Epoch {}/{}:'.format(epoch, num_epochs - 1), flush=True)

        summary_acc = 0
        for phase in ['train', 'val']:
            if phase == 'train':
                dataloader = train_dataloader
                scheduler.step()
                model.train()  # Set model to training mode
            else:
                dataloader = val_dataloader
                model.eval()   # Set model to evaluate mode

            running_loss = 0.
            running_acc = 0.

            for inputs, labels in tqdm(dataloader):
                inputs = inputs.to(device)
                labels = labels.to(device)

                optimizer.zero_grad()

                with torch.set_grad_enabled(phase == 'train'):
                    preds = model(inputs)
                    loss_value = loss(preds, labels)
                    preds_class = preds.argmax(dim=1)

                    if phase == 'train':
                        loss_value.backward()
                        optimizer.step()

                running_loss += loss_value.item()
                running_acc += (preds_class == labels.data).float().mean()

            epoch_loss = running_loss / len(dataloader)
            epoch_acc = running_acc / len(dataloader)
            summary_acc += epoch_acc

            print('{} Loss: {:.4f} Acc: {:.4f}'.format(phase, epoch_loss, epoch_acc), flush=True)

        if summary_acc/2 > best_summary_acc:
            best_summary_acc = summary_acc/2
            print(f"best accuracy achieved: {best_summary_acc}")
            torch.save(model.state_dict(), os.path.join(save_path, "lifestyle.pth"))

    return model


def train():
    data_root = 'dataset'

    train_dir = 'train'
    val_dir = 'val'

    class_names = ['trash', 'good']

    for dir in [train_dir, val_dir]:
        for class_name in class_names:
            full_path = os.path.join(dir, class_name)
            if not os.path.exists(full_path):
                os.makedirs(full_path)

    for class_name in class_names:
        source_dir = os.path.join(data_root, class_name)

        for i, file_name in enumerate(tqdm(os.listdir(source_dir))):
            if i % 6 != 0:
                dest_dir = os.path.join(train_dir, class_name)
            else:
                dest_dir = os.path.join(val_dir, class_name)
            shutil.copy(os.path.join(source_dir, file_name), os.path.join(dest_dir, file_name))

    train_transforms = transforms.Compose([
        transforms.RandomApply([
            transforms.ColorJitter(
                brightness=0.5,
                contrast=0.5,
                saturation=0.5,
                hue=0.5
            )
        ]),
        transforms.Resize(size=(224, 224)),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])

    val_transforms = transforms.Compose([
        transforms.Resize(size=(224, 224)),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])

    train_dataset = torchvision.datasets.ImageFolder(train_dir, train_transforms)
    val_dataset = torchvision.datasets.ImageFolder(val_dir, val_transforms)

    batch_size = 4

    train_dataloader = torch.utils.data.DataLoader(
        train_dataset, batch_size=batch_size, shuffle=True, num_workers=batch_size)
    val_dataloader = torch.utils.data.DataLoader(
        val_dataset, batch_size=batch_size, shuffle=False, num_workers=batch_size)

    model = models.resnet34(pretrained=True)

    for param in model.parameters():
        param.requires_grad = False

    model.fc = torch.nn.Linear(model.fc.in_features, 2)

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model = model.to(device)

    loss = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=1.0e-3)

    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=20, gamma=0.1)

    model = train_model(model=model, loss=loss, scheduler=scheduler, optimizer=optimizer, train_dataloader=
    train_dataloader, val_dataloader=val_dataloader, device=device, num_epochs=100)


def inference():
    ds_path = 'lifestyle_images'

    answers = {0: 'good', 1: 'trash'}

    transform = transforms.Compose([
        transforms.Resize(size=(224, 224)),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])

    model = models.resnet34(pretrained=False)

    for param in model.parameters():
        param.requires_grad = False

    model.fc = torch.nn.Linear(model.fc.in_features, 2)

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model = model.to(device)

    model.load_state_dict(torch.load('/volume/best_model/lifestyle_classification.pth'))
    model.eval()

    with torch.no_grad():
        for root, dirs, files in os.walk(ds_path):
            if files:
                for file in files:
                    full_path = os.path.join(root, file)

                    img = Image.open(full_path)
                    if len(img.size) == 2:
                        img = img.convert('RGB')

                    img_tensor = transform(img)

                    output = model(img_tensor.to(device)[None, ...])
                    label = np.argmax(output.cpu().detach().numpy()[0])
                    # print("label : ", label)

                    folder_to_save = os.path.join('/volume/cleaned/', str(label))
                    if not os.path.exists(folder_to_save):
                        os.makedirs(folder_to_save)
                    os.system(f"cp {full_path} {folder_to_save}")


if __name__ == "__main__":
    train()
    # inference()
